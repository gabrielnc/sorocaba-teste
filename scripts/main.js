jQuery(function () {
  structure.init();
});

var structure = {
  init: function () { }
};

var home = {
  init: function () {
    this.fadeInPage();
    this.slick();
    this.buttonTop();
    this.scrollTo();
    this.validSend();
  },

  slick: function () {
    $('.slick-home').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1500,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
          }
        },
        {
          breakpoint: 1024,
          settings: {
            infinite: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000,
          }
        },
        {
          breakpoint: 800,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000,
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000,
          }
        },
      ]
    });
  },

  buttonTop: function () {
    var btn = $('#button');
    $(window).scroll(function () {
      if ($(window).scrollTop() > 300) {
        btn.addClass('show');
      } else {
        btn.removeClass('show');
      }
    });

    btn.on('click', function (e) {
      e.preventDefault();
      $('html, body').animate({ scrollTop: 0 }, '300');
    });
  },

  scrollTo: function () {
    $('.scrollTo').click(function () {
      $('html, body').animate({
        scrollTop: $($(this).attr('href')).offset().top
      }, 500);
      return false;
    });
  },

  fadeInPage: function () {
    $(document).ready(function () {
      $('body').css('display', 'none');
      $('body').fadeIn(500);
    });
  },

  validSend: function () {
    
  },

};

